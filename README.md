# INF746 - Practical classes #

Clone this repository to a machine with the `Arduino* IDE` installed:

	$ git clone https://bitbucket.org/INF746/inf746edison/
	$ cd inf746edison/
	$ git submodule init
	$ git submodule update

The repository contains a `sketches/` directory that should be set as the
`Arduino* IDE` sketchbook. Start the IDE, go to `Preferences` and
update the `Sketchbook location` to `~/inf746edison/sketches`.
Confirm the change, and now menu `File > Sketchbook` should present two
entries: `libraries` and `tests`. They contain a number of sample
programs to be explored during the class.