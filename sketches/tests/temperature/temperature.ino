#include <Wire.h>
#include <TH02_dev.h>
#include "rgb_lcd.h"

rgb_lcd lcd;

int counter = 0;

void setup() {
	// LCD's number of columns and rows
	lcd.begin(16, 2);
}

void loop() {
	float temperature = TH02.ReadTemperature();
	lcd.clear();
	lcd.print("loop() [");
	lcd.print(++counter);
	lcd.print("]");
	lcd.setCursor(0, 1);
	lcd.print(" - Temp: ");
	lcd.print(temperature);
	lcd.print(" C");
	delay(1000);
}
