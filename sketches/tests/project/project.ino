/**
 * Autor     : minera38 - Marcos Vinícius Piaia
 * Descricao : o sistema a seguir implementa a simulação de um sistema de aquecimento usando os seguintes elementos: 
 *             - servo: indica qual elemento de aquecimento estah funcionando (gas, eletriciadade, nenhum == solr)
 *             - relay: chave que ativa ou desativa o sistema
 *             - botao: chave que indica se a agua para  casa estah aberta ou fechada
 *             - sensor de tempertura
 *             - sensor de humidade: usado no clculo d eficiencia do dispositivo
 *             - serial monitor: elemento do arduino que serah usado para o fornecimento de informcoes e setup do sistema
 */

#define DEBUGING 1

#define DEBUG(Y) if (DEBUGING) {Serial.print(__FUNCTION__); Serial.print(": "); Serial.println(Y);}
#define DEBUG_VAR(X) if (DEBUGING) {Serial.print(#X); Serial.print(": "); Serial.println(X);}

#define MAX_ACTIONSIZE 1 // usado para identificar o maximo de caracteres a serem usados na identificacao de uma acao
#define ACTION_COMMAND_SEPARATOR " " // usado para identificar o caracter que serah usado como separador dos elementos de uma acao


enum ERROR_TYPES_T {
    ERROR_TYPE_NOERROR = 0,
    ERROR_TYPE_INVALID_CMD_SEQUENCE,
    ERROR_TYPE_INVALID_CMD_VALUE,
    ERROR_TYPE_CMD_INCOMPLETE,
    ERROR_TYPE_UNKNOWN
};

enum COMMAND_STATE_TYPES_T {
    COMMAND_STATE_NOCMD = 0,  // estado inicial do sistema
    COMMAND_STATE_READING,    // comando sendo lido da serial
    COMMAND_STATE_RECEIVED,   // comando recebido e pronto para tratamento
    COMMAND_STATE_READY,      // sistema pronto para receber comandos
    COMMAND_STATE_UNKNOWN     // estado qdo commando invalido eh recebido
};

enum ACTION_TYPES_T {
    ACTION_NOACTION = 0,             // nenhuma acao a ser realizada
    ACTION_SET_GAS_COST,             // G
    ACTION_SET_ELECTRICITY_COST,     // E
    ACTION_SET_TARGET_TEMPERATURE,   // T
    ACTION_TOGGLE_REMOTE_BUTTON,     // R
    ACTION_TOGGLE_LOCAL_BUTTON,      // L
    ACTION_SET_WATER_TEMPERATURE,    // W
    ACTION_SET_MOISTURE_LEVEL,       // M
    ACTION_INVALIDACTION
};


/**
 * Global variables
 */
COMMAND_STATE_TYPES_T commandState = COMMAND_STATE_NOCMD;


/**
 * Functions
 */
void initialize_serial();
ERROR_TYPES_T handle_serial_commands();
ACTION_TYPES_T get_action(String commandParam);



/**
 * setup: funcao do sistema que eh executada durante sua incializacao para configurar e conectar as partes que o compoe
 */
void setup() {
    initialize_serial();
    printf("setup()\n");
}

/**
 * loop: funcao principal do framework do arduino no qual o 
 */
void loop() {
    ERROR_TYPES_T return_value = handle_serial_commands();
    DEBUG_VAR(return_value);
    delay(1000);
}

/**
 * handle_serial_commands: funcao usada para parsear os dados recebidos a partir da interface serial
 * return: ERROR_TYPES_T 
 */
ERROR_TYPES_T  handle_serial_commands() {
    ERROR_TYPES_T retValue = ERROR_TYPE_NOERROR;
    ACTION_TYPES_T actionType = ACTION_NOACTION;
    
    String command = "";
    char   inputRead;
    
    // verifica se existe informacao a ser lida
    while (Serial.available()) {
         inputRead = Serial.read();
         DEBUG_VAR(inputRead);
         // adiciona o caracter a cadeia de comando
         command += inputRead;
         commandState = COMMAND_STATE_READING; 
    }

    command.trim();

    if (command.length() > 0) {
        commandState = COMMAND_STATE_RECEIVED;
    }
    else {
        return ERROR_TYPE_CMD_INCOMPLETE;
    }
    // imprime command recebido
    DEBUG_VAR(command);

    // caso o comando nao tenha sido integralmente recebido
    if (commandState != COMMAND_STATE_RECEIVED) {
        return ERROR_TYPE_CMD_INCOMPLETE;
    }

    // obtem a acao informada
    actionType = get_action(command);
    
    return retValue;
}

/**
 * get_action: obtem a acao a ser executada
 */
ACTION_TYPES_T get_action(String commandParam) {

    // obtem a primeira parte da string
    int first_command = commandParam.indexOf(ACTION_COMMAND_SEPARATOR);

    DEBUG_VAR(first_command);
    // indica que nenhuma aca valida foi encontrada
    if (first_command <= 0){
        return ACTION_INVALIDACTION;
    }
    
    String action = commandParam.substring(0, first_command);

    DEBUG_VAR(action);

    // estamos usando uma string de acao para facilitar seu tratamento, sendo assim, nao temos
    // usar um case para o tratamento dos commandos.
    if (action.equalsIgnoreCase("G")) {
        return ACTION_SET_GAS_COST;
    }
    if (action.equalsIgnoreCase("E")) {
        return ACTION_SET_ELECTRICITY_COST;
    }
    if (action.equalsIgnoreCase("T")) {
        return ACTION_SET_TARGET_TEMPERATURE;
    }
    if (action.equalsIgnoreCase("R")) {
        return ACTION_TOGGLE_REMOTE_BUTTON;
    }
    if (action.equalsIgnoreCase("L")) {
        return ACTION_TOGGLE_LOCAL_BUTTON;
    }
    if (action.equalsIgnoreCase("W")) {
        return ACTION_SET_WATER_TEMPERATURE;
    }
    if (action.equalsIgnoreCase("M")) {
        return ACTION_SET_MOISTURE_LEVEL;
    }

    // se o fluxo de execucao chegou ateh aqui, nenhuma acao que eh controlada pelo sistema foi
    // identificada, entao retornando o INVALID ACTION
    DEBUG("Invalid Action");
    return ACTION_INVALIDACTION;
}

/**
 * intialize_serial: inicializa a porta serial
 */
void initialize_serial() {
    Serial.begin(9600);
}

