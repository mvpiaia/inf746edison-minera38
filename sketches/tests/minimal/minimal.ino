int counter = 0;

void setup() {
	printf("setup()\n");
	// flushes 'stdout' stream, since it is by default
	// redirected to the board's '/tmp/log.txt' file.
	fflush(stdout);
}

void loop() {
	printf("loop() [%d]\n", ++counter);
	fflush(stdout);
	delay(1000);
}
