#include <Wire.h>
#include "rgb_lcd.h"

rgb_lcd lcd;
const int buttonPin = 0;

int counter = 0;

void setup() {
	// LCD's number of columns and rows
	lcd.begin(16, 2);
	lcd.print("setup()");

	pinMode(buttonPin, INPUT);
}

void loop() {
	delay(1000);
        int buttonState = digitalRead(buttonPin);

	lcd.clear();
	lcd.print("loop() [");
	lcd.print(++counter);
	lcd.print("]");
	lcd.setCursor(0, 1);
	lcd.print("button: ");
	lcd.print(buttonState ? "down" : "up");
}
