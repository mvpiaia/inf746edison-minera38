#include <Wire.h>
#include "rgb_lcd.h"

rgb_lcd lcd;

int counter = 0;

void setup() {
	// LCD's number of columns and rows
	lcd.begin(16, 2);
	lcd.print("setup()");
}

void loop() {
	delay(1000);
	lcd.clear();
	lcd.print("loop() [");
	lcd.print(++counter);
	lcd.print("]");
}
