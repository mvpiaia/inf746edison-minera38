#define MAX_MESSAGE_LENGTH 80
char message[MAX_MESSAGE_LENGTH];

int counter = 0;

void setup() {
	/* Use as parameter the same baud rate
           adopted by the Serial Console */
	Serial.begin(9600);
	Serial.println("setup()");
}

void loop() {
	Serial.print("loop() [");
	Serial.print(++counter);
	Serial.println("]");
	while (readFromSerial()) {
		Serial.print("MESSAGE: '");
		Serial.print(message);
		Serial.println("'");
	}
	delay(1000);
}

bool readFromSerial() {
	int index = 0;
	while (Serial.available()) {
		message[index] = Serial.read();
		if ( (message[index] == '\n' && index > 0) ||
			(++index >= MAX_MESSAGE_LENGTH - 1) ) {
			message[index] = '\0';
			return true;
		}
	}
	return false;
}
